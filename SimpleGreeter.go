package greeter

import "fmt"

type SimpleGreeter struct {
}

func (o SimpleGreeter) Greet(name string) {
	fmt.Println("Hello " + name)
}
